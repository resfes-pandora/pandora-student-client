import React from "react";
import { Provider as StoreProvider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { configureStore } from "./redux/store";
import Login from "./views/Login";
import MainScreen from "./views/MainScreen";
import MediaProvider from "./components/MediaProvider";
import CheatDetector from "./components/CheatDetector";

const store = configureStore();

const App = () => {
  return (
    <MediaProvider>
      <StoreProvider store={store}>
        <Router>
          <Switch>
            <Route path="/main">
              <CheatDetector>
                <MainScreen />
              </CheatDetector>
            </Route>
            <Route path="/">
              <Login />
            </Route>
          </Switch>
        </Router>
      </StoreProvider>
    </MediaProvider>
  );
};

export default App;
