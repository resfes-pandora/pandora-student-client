import * as actionTypes from "../actions";

const initialState = {
  currentView: "WEBCAM",
  isOnline: true,
  isVolumeMuted: false,
};

const settingReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.VOLUME_MUTE_CHANGE: {
      return {
        ...state,
        isVolumeMuted: !state.isVolumeMuted,
      };
    }
    case actionTypes.WIFI_CHANGE: {
      return {
        ...state,
        isOnline: action.isOnline,
      };
    }
    case actionTypes.GO_TO_VIEW: {
      return {
        ...state,
        currentView: action.viewName,
      };
    }
    default: {
      return state;
    }
  }
};

export default settingReducer;
