import { combineReducers } from "redux";

import sessionReducer from "./sessionReducer";
import roomReducer from "./roomReducer";
import chatReducer from "./chatReducer";
import settingReducer from "./settingReducer";

const rootReducer = combineReducers({
  session: sessionReducer,
  room: roomReducer,
  chat: chatReducer,
  setting: settingReducer
});

export default rootReducer;
