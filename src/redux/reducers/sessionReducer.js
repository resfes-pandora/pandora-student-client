import * as actionTypes from "../actions";

const initialState = JSON.parse(localStorage.getItem("session")) || {
  user: {
    name: "",
    username: "",
    email: "",
    type: "STUDENT",
  },
  roomId: null,
  examCode: null,
  examDoingTime: null,
  examEndTime: null,
  isDoingExam: false,
};

const sessionReducer = (state = initialState, action) => {
  let newState;

  switch (action.type) {
    case actionTypes.SESSION_LOGIN:
      newState = {
        ...state,
        user: action.user,
        roomId: action.roomId,
      };
      break;
    case actionTypes.SESSION_LOGOUT:
      newState = {
        user: null,
        roomId: null,
      };
      break;
    case actionTypes.START_EXAM:
      newState = {
        ...state,
        isDoingExam: true,
        examCode: action.examCode,
        examDoingTime: action.examDoingTime,
        examEndTime: action.examEndTime,
      };
      break;
    case actionTypes.FINISH_EXAM:
      newState = {
        ...state,
        isDoingExam: false,
        examCode: null,
        examDoingTime: null,
        examEndTime: null,
      };
      break;

    default: {
      return state;
    }
  }

  localStorage.setItem("session", JSON.stringify(newState || state));
  return newState || state;
};

export default sessionReducer;
