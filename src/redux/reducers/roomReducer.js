import * as actionTypes from "../actions";

const initialState = {
  clients: [],
  joinedClient: null,
  disconnectedClient: null,
  newPeerId: null,
  newWebCamClientId: null,
  newScreenClientId: null,
  yourClient: null,
};

const roomReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.INIT_EXISTING_CLIENTS: {
      return {
        ...state,
        clients: action.clients,
        yourClient: action.yourClient,
      };
    }

    case actionTypes.ANOTHER_CLIENT_JOINED: {
      return {
        ...state,
        clients: [...state.clients, action.client],
        joinedClient: action.client,
      };
    }

    case actionTypes.ANOTHER_CLIENT_DISCONNECTED: {
      return {
        ...state,
        clients: state.clients.filter(
          (client) => client.id !== action.client.id
        ),
        disconnectedClient: action.client,
      };
    }

    case actionTypes.NEW_PEER_CONNECTED: {
      state.clients.find((client) => client.id === action.clientId).peerId =
        action.peerId;

      return {
        ...state,
        newPeerId: action.peerId,
      };
    }

    case actionTypes.NEW_WEBCAM_CONNECTED: {
      return {
        ...state,
        newWebCamClientId: action.clientId,
      };
    }

    case actionTypes.NEW_SCREEN_CONNECTED: {
      return {
        ...state,
        newScreenClientId: action.clientId,
      };
    }

    case actionTypes.NEW_EXAM_STATUS: {
      return {
        ...state,
        clients: state.clients.map((client) =>
          client.id === action.clientId
            ? { ...client, examStatus: action.examStatus }
            : client
        ),
      };
    }

    default: {
      return state;
    }
  }
};

export default roomReducer;
