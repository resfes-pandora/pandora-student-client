import * as actionTypes from "../actions";

const initialState = {
  currentMessage: null,
  currentNotification: null,
};

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.NEW_CHAT_MESSAGE: {
      return {
        ...state,
        currentMessage: action.message,
      };
    }

    case actionTypes.NEW_NOTIFICATION: {
      return {
        ...state,
        currentNotification: action.notification,
      };
    }

    default: {
      return state;
    }
  }
};

export default chatReducer;
