export const VOLUME_MUTE_CHANGE = "VOLUME_MUTE_CHANGE";
export const WIFI_CHANGE = "WIFI_CHANGE";
export const GO_TO_VIEW = "GO_TO_VIEW";

export const changeMuteStatus = (message) => async (dispatch) => {
  return dispatch({
    type: VOLUME_MUTE_CHANGE,
  });
};

export const changeOnlineStatus = (isOnline) => async (dispatch) => {
  return dispatch({
    type: WIFI_CHANGE,
    isOnline: isOnline,
  });
};

export const goToView = (viewName) => async (dispatch) => {
  return dispatch({
    type: GO_TO_VIEW,
    viewName: viewName,
  });
};