export const INIT_EXISTING_CLIENTS = "INIT_EXISTING_CLIENTS";
export const ANOTHER_CLIENT_JOINED = "ANOTHER_CLIENT_JOINED";
export const ANOTHER_CLIENT_DISCONNECTED = "ANOTHER_CLIENT_DISCONNECTED";
export const NEW_PEER_CONNECTED = "NEW_PEER_CONNECTED";
export const NEW_WEBCAM_CONNECTED = "NEW_WEBCAM_CONNECTED";
export const NEW_SCREEN_CONNECTED = "NEW_SCREEN_CONNECTED";
export const NEW_EXAM_STATUS = "NEW_EXAM_STATUS";

export const initExistingClients = (clients, yourClient) => async (
  dispatch
) => {
  return dispatch({
    type: INIT_EXISTING_CLIENTS,
    clients,
    yourClient,
  });
};

export const clientJoined = (client) => async (dispatch) => {
  return dispatch({
    type: ANOTHER_CLIENT_JOINED,
    client,
  });
};

export const clientDisconnected = (client) => async (dispatch) => {
  return dispatch({
    type: ANOTHER_CLIENT_DISCONNECTED,
    client,
  });
};

export const newPeerConnected = (clientId, peerId) => async (dispatch) => {
  return dispatch({
    type: NEW_PEER_CONNECTED,
    clientId,
    peerId,
  });
};

export const newWebCamConnected = (clientId) => async (dispatch) => {
  return dispatch({
    type: NEW_WEBCAM_CONNECTED,
    clientId,
  });
};

export const newScreenConnected = (clientId) => async (dispatch) => {
  return dispatch({
    type: NEW_SCREEN_CONNECTED,
    clientId,
  });
};

export const newExamStatus = (clientId, examStatus) => async (dispatch) => {
  return dispatch({
    type: NEW_EXAM_STATUS,
    clientId,
    examStatus
  });
};