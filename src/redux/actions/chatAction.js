export const NEW_CHAT_MESSAGE = "NEW_CHAT_MESSAGE";
export const NEW_NOTIFICATION = "NEW_NOTIFICATION";

export const newChatMessage = (message) => async (dispatch) => {
  return dispatch({
    type: NEW_CHAT_MESSAGE,
    message,
  });
};

export const newNotification = (notification) => async (dispatch) => {
  return dispatch({
    type: NEW_NOTIFICATION,
    notification,
  });
};
