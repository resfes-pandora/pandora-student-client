import { userLogin } from "../../services/apiService";
import { goToView } from "../../redux/actions";
import { getExam } from "../../services/apiService";
import { updateExamStatus } from "../../services/socketService"

export const SESSION_LOGIN = "SESSION_LOGIN";
export const SESSION_LOGOUT = "SESSION_LOGOUT";
export const START_EXAM = "START_EXAM";
export const FINISH_EXAM = "FINISH_EXAM";

export const login = ({ username, password, roomId }) => async (dispatch) => {
  const user = await userLogin(username, password);

  return dispatch({
    type: SESSION_LOGIN,
    user,
    roomId,
  });
};

export const logOut = () => async (dispatch) => {
  return dispatch({
    type: SESSION_LOGOUT,
  });
};

export const startExam = (examCode) => async (dispatch, getState) => {
  const data = await getExam(examCode);
  if (!data) return;
  const clientId = getState().room.yourClient.id;
  updateExamStatus(clientId, "DOING");
  dispatch(goToView("EXAM"));
  return dispatch({
    type: START_EXAM,
    examCode: data.examCode,
    examDoingTime: data.minutes * 60,
    examEndTime: Math.round(Date.now() / 1000) + (data.minutes * 60),
  });
};

export const finishExam = () => async (dispatch, getState) => {
  const clientId = getState().room.yourClient.id;
  updateExamStatus(clientId, "SUBMITTED");
  localStorage.clear();
  return dispatch({
    type: FINISH_EXAM,
  });
};
