import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles((theme) => ({
  member: {
    height: "60px",
    display: "flex",
    borderBottom: "1px solid #dbdbdb",
    minWidth: 0,
  },
  avatar: {
    flex: 3,
    flexShrink: 0,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.05)",
  },
  name: {
    flex: 7,
    display: "flex",
    alignItems: "center",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    paddingLeft: "3%",
  },
  faceIcon: {
    color: "gray",
    fontSize: "45px",
  },
}));

const getShortName = (name) => {
  const words = name.split(" ");
  return words[0].split("")[0] + words[words.length - 1].split("")[0];
};

const getColor = (name) => {
  const colors = ["#836359", "#F04724", "#EF386F", "#004897", "#744AB7"];
  return colors[name.length % 5];
};

const Member = ({ client }) => {
  const classes = useStyles();

  return (
    <div className={classes.member}>
      <div className={classes.avatar}>
        <Avatar
          style={{
            backgroundColor: getColor(client.name),
          }}
        >
          {getShortName(client.name)}
        </Avatar>
      </div>
      <div
        className={classes.name}
        style={{ color: client.type === "SUPERVISOR" ? "red" : "black" }}
      >
        {client.name}
      </div>
    </div>
  );
};

export default Member;
