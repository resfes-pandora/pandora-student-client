import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { initCheatDetector } from "../../services/cheatDetectorService";
import { testAiModel } from "../../services/aiService";
import { makeStyles } from "@material-ui/core/styles";
import LoadingPage from "../../components/LoadingPage";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));

// eslint-disable-next-line
const CheatDetector = ({ children }) => {
  const classes = useStyles();
  const user = useSelector((state) => state.session.user);
  const isDoingExam = useSelector((state) => state.session.isDoingExam);
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    if (user) {
      if (user.type === "STUDENT") {
        console.log("AI PASSING...");

        testAiModel(user).then(() => {
          setIsReady(true);
        });
      } else {
        setIsReady(true);
      }
    }
  }, [user]);

  useEffect(() => {
    if (user && user.type === "STUDENT" && isDoingExam) {
      console.log("CHEAT DETECTOR WORKING...");

      initCheatDetector(user).then(() => {
        setIsReady(true);
      });
    }
  }, [user, isDoingExam]);

  return (
    <div className={classes.root}>{isReady ? children : <LoadingPage />}</div>
  );
};

export default CheatDetector;
