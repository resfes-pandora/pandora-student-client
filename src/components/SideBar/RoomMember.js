import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { Member } from "../RoomMember";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  container: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  chatView: {
    flex: 1,
    overflowY: "scroll",
  },
}));

const RoomMember = (props) => {
  const classes = useStyles();
  const clients = useSelector((state) => state.room.clients);

  return (
    <div className={classes.container}>
      <div className={classes.chatView}>
        {clients.map((client) => (
          <Member key={client.id} client={client} />
        ))}
      </div>
    </div>
  );
};

export default RoomMember;
