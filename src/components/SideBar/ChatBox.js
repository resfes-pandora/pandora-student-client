import React, { useCallback, useRef, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { IconButton } from "@material-ui/core";
import SendIcon from "@material-ui/icons/Send";
import { ChatItem } from "../../components/Chat";
import { sendChatMessage } from "../../services/socketService.js";
import ScrollToBottom from "react-scroll-to-bottom";

const useStyles = makeStyles((theme) => ({
  container: {
    height: "80%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
  },
  chatView: {
    flex: 1,
    overflowY: "auto",
  },
  chatInputView: {
    height: "50px",
    display: "flex",
    border: "1px solid #C8C8C8",
    justifyContent: "center",
    alignItems: "center",
  },
  textBox: {
    width: "81%",
    height: "70%",
    borderRadius: "5px",
    border: "1px solid #C8C8C8",
    fontSize: "16px",
    paddingLeft: "10px",
    marginLeft: "2%",
    "&:focus": {
      outline: "none",
    },
  },
  sendBtn: {
    borderRadius: "6px",
    paddingLeft: "1%",
    paddingRight: "2%",
  },
  sendIcon: {
    color: "#464F51",
    fontSize: "40px",
  },
}));

const ChatBox = (props) => {
  const classes = useStyles();

  const chatInput = useRef(null);
  const [chatMessages, setChatMessages] = useState([]);
  const chatMessage = useSelector((state) => state.chat.currentMessage);

  useEffect(() => {
    if (chatMessage === null) return;
    setChatMessages([...chatMessages, chatMessage]);
  }, [chatMessage]); // eslint-disable-line react-hooks/exhaustive-deps

  const sendMessage = useCallback((e) => {
    if (chatInput.current.value.length === 0) return;
    sendChatMessage(chatInput.current.value);
    chatInput.current.value = "";
  }, []);

  const onKeyUp = useCallback(
    (e) => {
      if (e.keyCode === 13) sendMessage();
    },
    [sendMessage]
  );

  return (
    <div
      className={classes.container}
      style={{ display: props.value !== props.index && "none" }}
    >
      <ScrollToBottom className={classes.chatView} behavior="scroll">
        {chatMessages.map((message, index) => (
          <ChatItem key={index} {...message} />
        ))}
      </ScrollToBottom>
      <div className={classes.chatInputView}>
        <input className={classes.textBox} ref={chatInput} onKeyUp={onKeyUp} />
        <IconButton className={classes.sendBtn} onClick={sendMessage}>
          <SendIcon className={classes.sendIcon} />
        </IconButton>
      </div>
    </div>
  );
};

export default React.memo(ChatBox);
