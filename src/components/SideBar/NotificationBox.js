import { makeStyles } from "@material-ui/core/styles";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import ScrollToBottom from "react-scroll-to-bottom";
import { NotificationItem } from "../../components/Notification";

const useStyles = makeStyles((theme) => ({
  container: {
    height: "80%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
  },
  notificationView: {
    flex: 1,
    overflowY: "auto",
  },
}));

const NotificationBox = (props) => {
  const classes = useStyles();

  const [notifications, setNotifications] = useState([]);
  const notification = useSelector(
    (state) => state.chat.currentNotification
  );

  useEffect(() => {
    if (notification === null) return;
    setNotifications([...notifications, notification]);
  }, [notification]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div
      className={classes.container}
      style={{ display: props.value !== props.index && "none" }}
    >
      <ScrollToBottom className={classes.notificationView} behavior="scroll">
        {notifications.map((notification, index) => (
          <NotificationItem key={index} {...notification} />
        ))}
      </ScrollToBottom>
    </div>
  );
};

export default React.memo(NotificationBox);
