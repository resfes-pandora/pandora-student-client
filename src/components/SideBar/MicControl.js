import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { IconButton } from "@material-ui/core";
import MicIcon from "@material-ui/icons/Mic";
import MicOffIcon from "@material-ui/icons/MicOff";
import { useDispatch, useSelector } from "react-redux";
import { changeMuteStatus } from "../../redux/actions/settingAction";

const useStyles = makeStyles((theme) => ({
  container: {
    flex: "0 0 15%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#464F51",
  },
  whiteText: {
    color: "white",
  },
  iconBtn: {
    width: "19%",
    color: "white",
    borderRadius: "6px",
    paddingTop: "4px",
    paddingBottom: "4px",
    marginLeft: "3%",
  },
  micBtn: {
    background: "linear-gradient(to right, #36DE8A, #36DE8A)",
  },
  micOffBtn: {
    background: "linear-gradient(to right, #FF5858, #FF5858)",
  },
  icon: {
    fontSize: "35px",
  },
}));

const MicControl = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const isVolumeMuted = useSelector((state) => state.setting.isVolumeMuted);
  const micStyle = isVolumeMuted ? classes.micOffBtn : classes.micBtn;

  return (
    <div className={classes.container}>
      <h2 className={classes.whiteText}>Room mic:</h2>
      <IconButton
        className={[classes.iconBtn, micStyle].join(" ")}
        onClick={() => dispatch(changeMuteStatus())}
      >
        {isVolumeMuted ? (
          <MicOffIcon className={classes.icon} />
        ) : (
          <MicIcon className={classes.icon} />
        )}
      </IconButton>
    </div>
  );
};

export default MicControl;
