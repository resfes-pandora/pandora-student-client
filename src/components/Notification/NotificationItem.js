import { makeStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    minHeight: "50px",
    padding: "10px",
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    paddingBottom: "5px",
  },
  name: {
    fontWeight: "bold",
  },
  time: {
    alignSelf: "self-end",
  },
  content: {
    fontSize: "15px",
    color: "#C80000",
  },
}));

const NotificationItem = (props) => {
  const classes = useStyles();
  if(props==={}) return null;

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <h4 >{props.user.name}</h4>
        <h4 className={classes.time}>
          {new Date().toLocaleTimeString().replace(/:\d\d\s/g, " ")}
        </h4>
      </div>
      <p className={classes.content}>{props.type}</p>
    </div>
  );
};

export default React.memo(NotificationItem);
