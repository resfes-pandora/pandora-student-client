import Peer from "peerjs";
import React, { useEffect, useReducer } from "react";
import { setPeerId } from "../../services/socketService";
import { useMediaContext } from "../MediaProvider";
import { useStreamContext } from "./StreamProvider";

const peerReducer = (state, action) => {
  switch (action.type) {
    case "INIT_PEER":
      return action.peer;

    default:
      return state;
  }
};

const peerActions = {
  initPeer: (peer) => ({
    type: "INIT_PEER",
    peer,
  }),
};

const StateContext = React.createContext(null);
const DispatchContext = React.createContext();

export const usePeerContext = () => [
  React.useContext(StateContext),
  React.useContext(DispatchContext),
  peerActions,
];

const startPeer = (mediaState, streamDispatch, streamActions) => {
  const peer = new Peer(undefined, {
    host: "peerjs-server.herokuapp.com",
    secure: true,
    port: 443,
  });

  peer.on("open", (id) => {
    setPeerId(id);
  });

  peer.on("call", (call) => {
    const { sourceClientId, type } = call.metadata;

    call.answer(
      type === "WEBCAM" ? mediaState.webCamStream : mediaState.screenStream
    );

    call.on("stream", (userVideoStream) => {
      if (type === "WEBCAM") {
        streamDispatch(
          streamActions.setWebCamStream(sourceClientId, userVideoStream)
        );
      } else if (type === "SCREEN") {
        streamDispatch(
          streamActions.setScreenStream(sourceClientId, userVideoStream)
        );
      }
    });
  });

  return peer;
};

const PeerProvider = ({ children }) => {
  const [mediaState] = useMediaContext();
  const [, streamDispatch, streamActions] = useStreamContext();
  const [state, dispatch] = useReducer(peerReducer, null);

  useEffect(() => {
    if (streamDispatch && streamActions && mediaState.webCamStream) {
      const peer = startPeer(mediaState, streamDispatch, streamActions);
      dispatch(peerActions.initPeer(peer));
    }
  }, [mediaState, streamDispatch, streamActions]);

  return (
    <StateContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        {children}
      </DispatchContext.Provider>
    </StateContext.Provider>
  );
};

export default PeerProvider;
