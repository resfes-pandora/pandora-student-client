import React, { useRef, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useStreamContext } from "./StreamProvider";
import { useSelector } from "react-redux";

const useStyles = makeStyles(() => ({
  stream: {
    // display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  },
  video: {
    // width: "640px",
    // height: "360px",
    backgroundColor: "black",
  },
  info: {
    position: "relative",
    marginLeft: "10px",
    marginTop: "-30px",
    marginBottom: "7px",
    color: "red",
    textShadow:
      "2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff",
    // backgroundColor: "white",
  },
}));

const calculateSize = (width, height) => {
  const OFFSET = 16 / 9;

  if (width / height > OFFSET) {
    return [height * OFFSET, height];
  } else {
    return [width, width / OFFSET];
  }
};

const Stream = ({ client, stream, streamType }) => {
  const classes = useStyles();
  const videoRef = useRef();

  const currentView = useSelector((state) => state.setting.currentView);
  const isVolumeMuted = useSelector((state) => state.setting.isVolumeMuted);
  const [size, setSize] = useState([640, 360]);
  const [streamState] = useStreamContext();

  useEffect(() => {
    if (stream) {
      videoRef.current.srcObject = stream;
    }
  }, [stream]);

  useEffect(() => {
    const totalStream =
      streamState.filter((client) => client.type === "STUDENT").length *
      (currentView === "WEBCAM-SCREEN" ? 2 : 1);
    const contentArea = document.getElementById("contentArea");

    const containerWidth = contentArea.offsetWidth;
    const containerHeight = contentArea.offsetHeight;

    if (totalStream === 1) {
      setSize(calculateSize(containerWidth, containerHeight));
    } else if (totalStream === 2) {
      setSize(calculateSize(containerWidth / 2, containerHeight));
    } else if (totalStream <= 4) {
      setSize(calculateSize(containerWidth / 2, containerHeight / 2));
    } else if (totalStream <= 6) {
      setSize(calculateSize(containerWidth / 3, containerHeight / 2));
    } else if (totalStream <= 8) {
      setSize(calculateSize(containerWidth / 4, containerHeight / 2));
    } else if (totalStream <= 9) {
      setSize(calculateSize(containerWidth / 3, containerHeight / 3));
    } else if (totalStream <= 12) {
      setSize(calculateSize(containerWidth / 4, containerHeight / 3));
    } else {
      setSize(calculateSize(containerWidth / 4, containerHeight / 4));
    }
  }, [streamState, currentView]);

  return (
    <div
      className={classes.stream}
      hidden={streamType !== currentView && currentView !== "WEBCAM-SCREEN"}
    >
      <video
        className={classes.video}
        style={{ width: size[0], height: size[1] }}
        autoPlay
        muted={client.isMuted || isVolumeMuted}
        ref={videoRef}
      ></video>
      <h4 className={classes.info}>{client.name}</h4>
    </div>
  );
};

export default Stream;
