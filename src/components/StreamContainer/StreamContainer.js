/* eslint-disable */
import { makeStyles } from "@material-ui/core/styles";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useMediaContext } from "../MediaProvider";
import { usePeerContext } from "./PeerProvider";
import Stream from "./Stream";
import { useStreamContext } from "./StreamProvider";

const useStyles = makeStyles(() => ({
  root: {
    height: "100%",
    display: "flex",
    flexFlow: "row wrap",
    alignItems: "center",
    justifyContent: "space-around",
  },
}));

const StreamContainer = () => {
  const classes = useStyles();

  const [streamState, streamDispatch, streamActions] = useStreamContext();
  const [peer] = usePeerContext();
  const [mediaState] = useMediaContext();

  const currentView = useSelector((state) => state.setting.currentView);
  const clients = useSelector((state) => state.room.clients);
  const yourClient = useSelector((state) => state.room.yourClient);
  const newPeerId = useSelector((state) => state.room.newPeerId);

  const newWebCamClientId = useSelector(
    (state) => state.room.newWebCamClientId
  );
  const newScreenClientId = useSelector(
    (state) => state.room.newScreenClientId
  );

  useEffect(() => {
    if (newPeerId && peer) {
      const clientStream = clients.find(
        (client) => client.peerId === newPeerId
      );

      try {
        const webCamCall = peer.call(
          clientStream.peerId,
          mediaState.webCamStream,
          {
            metadata: {
              sourceClientId: yourClient.id,
              type: "WEBCAM",
            },
          }
        );

        webCamCall.on("stream", (userVideoStream) => {
          streamDispatch(
            streamActions.setWebCamStream(clientStream.id, userVideoStream)
          );
        });
      } catch (error) {}

      try {
        const screenCall = peer.call(
          clientStream.peerId,
          mediaState.screenStream,
          {
            metadata: {
              sourceClientId: yourClient.id,
              type: "SCREEN",
            },
          }
        );

        screenCall.on("stream", (userVideoStream) => {
          streamDispatch(
            streamActions.setScreenStream(clientStream.id, userVideoStream)
          );
        });
      } catch (error) {}
    }
  }, [newPeerId, peer]);

  // useEffect(() => {
  //   if (newWebCamClientId && peer) {
  //     const clientStream = clients.find(
  //       (client) => client.id === newScreenClientId
  //     );

  //     try {
  //       const webCamCall = peer.call(
  //         clientStream.peerId,
  //         mediaState.webCamStream,
  //         {
  //           metadata: {
  //             sourceClientId: yourClient.id,
  //             type: "WEBCAM",
  //           },
  //         }
  //       );

  //       webCamCall.on("stream", (userVideoStream) => {
  //         streamDispatch(
  //           streamActions.setWebCamStream(clientStream.id, userVideoStream)
  //         );
  //       });
  //     } catch (error) {}
  //   }
  // }, [newWebCamClientId, peer]);

  // useEffect(() => {
  //   if (newScreenClientId && peer) {
  //     const clientStream = clients.find(
  //       (client) => client.id === newScreenClientId
  //     );

  //     try {
  //       const screenCall = peer.call(
  //         clientStream.peerId,
  //         mediaState.screenStream,
  //         {
  //           metadata: {
  //             sourceClientId: yourClient.id,
  //             type: "SCREEN",
  //           },
  //         }
  //       );

  //       screenCall.on("stream", (userVideoStream) => {
  //         streamDispatch(
  //           streamActions.setScreenStream(clientStream.id, userVideoStream)
  //         );
  //       });
  //     } catch (error) {}
  //   }
  // }, [newScreenClientId, peer]);

  return (
    <div className={classes.root}>
      {streamState
        .filter((client) => client.type === "STUDENT")
        .map((client) => {
          return [
            <Stream
              key={client.id + "webCam"}
              client={client}
              stream={client.webCamStream}
              streamType="WEBCAM"
            />,
            <Stream
              key={client.id + "screen"}
              client={client}
              stream={client.screenStream}
              streamType="SCREEN"
            />,
          ];
        })}
    </div>
  );
};

export default StreamContainer;
