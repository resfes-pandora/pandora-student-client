/* eslint-disable */
import React, { useReducer, useEffect } from "react";
import { useSelector } from "react-redux";
import { useMediaContext } from "../MediaProvider";
import {
  setWebCamConnected,
  setScreenConnected,
} from "../../services/socketService";

const streamReducer = (state, action) => {
  switch (action.type) {
    case "INIT_CLIENTS":
      return [...action.clients];

    case "ADD_CLIENT":
      return [...state, action.client];

    case "REMOVE_CLIENT":
      return [...state.filter((client) => client.id !== action.clientId)];

    case "SET_WEBCAM_STREAM":
      state.find((client) => client.id === action.clientId).webCamStream =
        action.webCamStream;
      return [...state];

    case "SET_SCREEN_STREAM":
      state.find((client) => client.id === action.clientId).screenStream =
        action.screenStream;
      return [...state];

    case "MUTED":
      state.find((client) => client.id === action.clientId).isMuted = true;
      return [...state];

    default:
      return state;
  }
};

const streamActions = {
  initClients: (clients) => ({
    type: "INIT_CLIENTS",
    clients,
  }),
  setWebCamStream: (clientId, webCamStream) => ({
    type: "SET_WEBCAM_STREAM",
    clientId,
    webCamStream,
  }),
  setScreenStream: (clientId, screenStream) => ({
    type: "SET_SCREEN_STREAM",
    clientId,
    screenStream,
  }),
  addClient: (client) => ({
    type: "ADD_CLIENT",
    client,
  }),
  removeClient: (clientId) => ({
    type: "REMOVE_CLIENT",
    clientId,
  }),
  muted: (clientId) => ({
    type: "MUTED",
    clientId,
  }),
};

const StateContext = React.createContext([]);
const DispatchContext = React.createContext();

export const useStreamContext = () => [
  React.useContext(StateContext),
  React.useContext(DispatchContext),
  streamActions,
];

const StreamProvider = ({ children }) => {
  const [state, dispatch] = useReducer(streamReducer, []);
  const [mediaState] = useMediaContext();

  const clients = useSelector((state) => state.room.clients);
  const yourClient = useSelector((state) => state.room.yourClient);
  const joinedClient = useSelector((state) => state.room.joinedClient);
  const disconnectedClient = useSelector(
    (state) => state.room.disconnectedClient
  );

  // Handle init clients
  useEffect(() => {
    if (yourClient) {
      dispatch(streamActions.initClients(clients));

      dispatch(streamActions.muted(yourClient.id));
    }
  }, [yourClient]);

  useEffect(() => {
    if (yourClient && mediaState.webCamStream) {
      dispatch(
        streamActions.setWebCamStream(yourClient.id, mediaState.webCamStream)
      );
      setWebCamConnected(yourClient.id);
    }
  }, [yourClient, mediaState.webCamStream]);

  useEffect(() => {
    if (yourClient && mediaState.screenStream) {
      dispatch(
        streamActions.setScreenStream(yourClient.id, mediaState.screenStream)
      );

      setScreenConnected(yourClient.id);
    }
  }, [yourClient, mediaState.screenStream]);

  // Handle when a client is disconnected
  useEffect(() => {
    if (disconnectedClient) {
      dispatch(streamActions.removeClient(disconnectedClient.id));
    }
  }, [disconnectedClient]);

  // Handle when a client is joined
  useEffect(() => {
    if (joinedClient) {
      dispatch(streamActions.addClient(joinedClient));
    }
  }, [joinedClient]);

  return (
    <StateContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        {children}
      </DispatchContext.Provider>
    </StateContext.Provider>
  );
};

export default StreamProvider;
