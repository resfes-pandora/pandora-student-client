import React from "react";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";

const CustomButton = withStyles((theme) => ({
  root: {
    height: "44px",
    width: "50px",
    color: "#FFF",
    padding: 0,
    margin: "0 3px",
  },
  label: {
    flexDirection: "column",
  },
}))((props) => {
  const {children, ...btnProps} = props;
  return <Button {...btnProps}>{props.children}</Button>;
});

export default CustomButton;
