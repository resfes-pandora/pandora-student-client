import { makeStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    minHeight: "50px",
    padding: "10px",
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    paddingBottom: "5px",
  },
  name: {
    fontWeight: "bold",
  },
  supervisorName: {
    color: "#C80000",
  },
  time: {
    alignSelf: "self-end",
  },
  content: {
    fontSize: "15px",
  },
}));

const ChatItem = (props) => {
  const classes = useStyles();
  const nameStyle = props.isSupervisor ? classes.supervisorName : {};

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <h4 className={nameStyle}>{props.clientName}</h4>
        <h4 className={classes.time}>
          {new Date().toLocaleTimeString().replace(/:\d\d\s/g, " ")}
        </h4>
      </div>
      <p className={classes.content}>{props.message}</p>
    </div>
  );
};

export default React.memo(ChatItem);
