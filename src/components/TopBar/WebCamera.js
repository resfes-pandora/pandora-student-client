import { makeStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  test: {
    minWidth: "170px",
    backgroundImage: `url("https://picsum.photos/185/100")`,
    backgroundPosition: "center",
    backgroundRepreat: "no-repeat",
    backgroundSize: "cover",
  },
}));

const WebCamera = (props) => {
  const classes = useStyles();

  return <div className={classes.test}></div>;
};

export default WebCamera;
