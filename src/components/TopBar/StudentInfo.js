import { makeStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  box: {
    display: "flex",
    width: "20rem",
    justifyContent: "center",
    alignItems: "center",
  },
  leftSideBox: {
    display: "flex",
    textAlign: "right",
    flexDirection: "column",
    paddingRight: ".6rem",
  },
  rightSideBox: {},
}));

const StudentInfo = ({ user }) => {
  const classes = useStyles();

  const type = user.type==="STUDENT" ? "Student" : "Supervisor";

  return (
    <div className={classes.box}>
      <div className={classes.leftSideBox}>
        <strong>{type}</strong>
        <strong>ID:</strong>
        <strong>Email:</strong>
        <strong>{user.type==="STUDENT" ? "Supervisor:" : "Role:"}</strong>
      </div>
      <div className={classes.rightSideBox}>
        <p>{user.name}</p>
        <p>{user.username}</p>
        <p>{user.email}</p>
        <p>{user.type==="STUDENT" ? "Nguyễn Đăng Nghĩa" : type}</p>
      </div>
    </div>
  );
};

export default StudentInfo;
