import React, { useState, useEffect } from "react";

const TimeCounter = ({ time, isDoingExam, endExam }) => {

  const [seconds, setSeconds] = useState(0);
  const [minutes, setMinutes] = useState(0);

  useEffect(() => {
    setMinutes(Math.floor(time / 60));
    setSeconds(time % 60);
  }, [time]);

  useEffect(() => {
    if (isDoingExam) {
      const interval = setInterval(() => {
        if (seconds > 0) {
          setSeconds((seconds) => seconds - 1);
        } else if (minutes > 0) {
          setMinutes((minutes) => minutes - 1);
          setSeconds(59);
        } else {
          clearInterval(interval);
          endExam();
        }
      }, 1000);
      return () => clearInterval(interval);
    } else {
      setMinutes(0);
      setSeconds(0);
      return () => {};
    }
  }, [minutes, seconds, isDoingExam, endExam]);

  return (
    <strong>
      {minutes}:{seconds}
    </strong>
  );
};

export default TimeCounter;
