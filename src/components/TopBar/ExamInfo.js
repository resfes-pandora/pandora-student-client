import { makeStyles } from "@material-ui/core/styles";
import React from "react";
// import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  box: {
    display: "flex",
    width: "20rem",
    justifyContent: "center",
    alignItems: "center",
  },
  leftSideBox: {
    display: "flex",
    textAlign: "right",
    flexDirection: "column",
    paddingRight: ".6rem",
  },
}));

const ExamInfo = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.box}>
      <div className={classes.leftSideBox}>
        <strong>Server:</strong>
        <strong>Client:</strong>
        <strong>Exam code:</strong>
        <strong>Total time:</strong>
      </div>
      <div>
        <p>Pandora_FU_ENG</p>
        <p>STD_v0.1.5</p>
        <p>MONKEY_DONKEY</p>
        <p>{props.totalTime} minutes</p>
      </div>
    </div>
  );
};

export default ExamInfo;
