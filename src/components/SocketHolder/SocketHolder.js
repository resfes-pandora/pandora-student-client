import React, { useEffect } from "react";
import { initSocket } from "../../services/socketService";
import { useDispatch } from "react-redux";

const SocketHolder = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const socket = initSocket(dispatch);

    return () => {
      socket.disconnect();
    };
  }, [dispatch]);

  return <div />;
};

export default SocketHolder;
