import React, { useReducer, useEffect } from "react";
// const electron = window.require("electron");

const initialState = {
  webCamStream: null,
  screenStream: null,
};

const StateContext = React.createContext(initialState);

export const useMediaContext = () => [React.useContext(StateContext)];

const MediaProvider = ({ children }) => {
  const [state, dispatch] = useReducer(
    (state, newValue) => ({ ...state, ...newValue }),
    initialState
  );

  useEffect(() => {
    navigator.mediaDevices
      .getUserMedia({
        video: {
          width: 320,
          height: 180,
          frameRate: 20,
        },
        audio: true,
      })
      .then((webCamStream) => {
        dispatch({ webCamStream });
      })
      .catch((error) => console.log(error));

    try {
      const { desktopCapturer } = window.require("electron");

      desktopCapturer.getSources({ types: ["screen"] }).then((sources) => {
        navigator.mediaDevices
          .getUserMedia({
            audio: false,
            video: {
              mandatory: {
                chromeMediaSource: "desktop",
                chromeMediaSourceId: sources[0].id,
                maxWidth: 640,
                minWidth: 320,
                maxHeight: 360,
                minHeight: 180,
              },
            },
          })
          .then((screenStream) => {
            dispatch({ screenStream });
          })
          .catch((error) => console.log(error));
      });
    } catch {
      navigator.mediaDevices
        .getDisplayMedia({
          video: {
            width: 640,
            height: 360,
            frameRate: 10,
          },
          audio: false,
        })
        .then((screenStream) => {
          dispatch({ screenStream });
        })
        .catch((error) => console.log(error));
    }
  }, []);

  return (
    <StateContext.Provider value={state}>{children}</StateContext.Provider>
  );
};

export default MediaProvider;
