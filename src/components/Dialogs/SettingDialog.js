import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { goToView, logOut } from "../../redux/actions";
import DialogTitle from "./DialogTitle";
import SettingItem from "./SettingItem";
import MeetingRoomIcon from "@material-ui/icons/MeetingRoom";
import { useHistory } from "react-router-dom";

const DialogContent = withStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(4),
  },
}))(MuiDialogContent);

const useStyles = makeStyles((theme) => ({
  switchBtn: {
    color: "#FFF",
    background: "linear-gradient(to right, #0be382, #0be382)",
    fontWeight: "bold",
  },
  logOutBtn: {
    color: "#FFF",
    background: "linear-gradient(to right, #FF0048, #FF0048)",
    fontWeight: "bold",
    margin: "0px 13px 7px 0px",
  },
  viewName: {
    color: "#0be382",
  },
}));

const viewItems = [
  {
    name: "Exam View",
    view: "EXAM",
    type: ["STUDENT"],
  },
  {
    name: "Camera-Screen View",
    view: "WEBCAM-SCREEN",
    type: ["SUPERVISOR"],
  },
  {
    name: "Camera View",
    view: "WEBCAM",
    type: ["STUDENT", "SUPERVISOR"],
  },
  {
    name: "Screen View",
    view: "SCREEN",
    type: ["SUPERVISOR"],
  },
];

const SettingDialog = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const currentView = useSelector((state) => state.setting.currentView);
  const user = useSelector((state) => state.session.user);

  const handleGoToView = useCallback(
    (view) => {
      dispatch(goToView(view));
      props.onClose();
    },
    [dispatch, props]
  );

  const onLogOutBtnClicked = useCallback(() => {
    dispatch(logOut());
    history.push("/");
  }, [dispatch, history]);

  const goToViewItems = viewItems.reduce((acc, item) => {
    if (item.view !== currentView && item.type.includes(user.type)) {
      acc.push([
        <SettingItem>
          <p>
            {"Go to "}
            <span className={classes.viewName}>{item.name}</span>
          </p>
          <Button
            className={classes.switchBtn}
            onClick={() => handleGoToView(item.view)}
          >
            Go
          </Button>
        </SettingItem>,
        <br />,
      ]);
    }

    return acc;
  }, []);

  return (
    <Dialog
      open={props.open}
      className={classes.root}
      classes={{ paper: classes.paper }}
      fullWidth={true}
      maxWidth="xs"
    >
      <DialogTitle id="customized-dialog-title" onClose={props.onClose}>
        Settings
      </DialogTitle>
      <DialogContent>{goToViewItems}</DialogContent>
      <MuiDialogActions>
        <Button
          variant="contained"
          className={classes.logOutBtn}
          startIcon={<MeetingRoomIcon />}
          onClick={onLogOutBtnClicked}
        >
          Log Out
        </Button>
      </MuiDialogActions>
    </Dialog>
  );
};

export default SettingDialog;
