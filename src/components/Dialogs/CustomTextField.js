import React from "react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const CustomTextField = withStyles({
  root: {
    minWidth: "260px",
    "& .MuiInputBase-input": {
      height: "5px",
    },
    "& .MuiOutlinedInput-root": {
      "&.Mui-focused fieldset": {
        borderColor: "#FF7B55",
      },
      "&:hover fieldset": {
        borderColor: "#FF7B55",
      },
    },
  },
})((props) => <TextField {...props} />);

export default CustomTextField;
