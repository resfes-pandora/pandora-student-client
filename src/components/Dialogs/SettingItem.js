import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    fontSize: "20px",
    fontWeight: "bold",
  },
}));

const SettingItem = (props) => {
  const classes = useStyles();
  return <div className={classes.root}>{props.children}</div>;
};

export default SettingItem;
