import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import React from "react";
import { useSelector } from "react-redux";
import DialogTitle from "./DialogTitle";
import SettingItem from "./SettingItem";

const DialogContent = withStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(4),
  },
}))(MuiDialogContent);

const StatusButton = withStyles((theme) => ({
  root: {
    width: "100px",
  },
}))(Button);

const useStyles = makeStyles(() => ({
  notYetBtn: {
    color: "#989898",
    background: "linear-gradient(to right, #E9E9E9, #E9E9E9)",
    fontWeight: "bold",
  },
  doingBtn: {
    color: "#FFF",
    background: "linear-gradient(to right, #0be382, #0be382)",
    fontWeight: "bold",
  },
  submittedBtn: {
    color: "#FFF",
    background: "linear-gradient(to right, #ff7b55, #ff7b55)",
    fontWeight: "bold",
  },
  marginBottom: {
    marginBottom: "20px",
  },
  paper: { minHeight: "600px", maxHeight: "600px" },
}));

const StudentTrackingDialog = (props) => {
  const classes = useStyles();

  const clients = useSelector((state) => state.room.clients);

  const listStudents = clients
    .filter((client) => client.type === "STUDENT")
    .map((client) => {
      let statusBtn = null;
      switch (client.examStatus) {
        case "NOT_YET_START":
          statusBtn = (
            <StatusButton className={classes.notYetBtn}>Not Yet</StatusButton>
          );
          break;
        case "DOING":
          statusBtn = (
            <StatusButton className={classes.doingBtn}>Doing</StatusButton>
          );
          break;
        case "SUBMITTED":
          statusBtn = (
            <StatusButton className={classes.submittedBtn}>
              Submitted
            </StatusButton>
          );
          break;
        default:
          break;
      }
      return (
        <div key={client.id} className={classes.marginBottom}>
          <SettingItem>
            <p>{client.name}</p>
            {statusBtn}
          </SettingItem>
        </div>
      );
    });

  return (
    <Dialog
      open={props.open}
      className={classes.root}
      classes={{ paper: classes.paper }}
      fullWidth={true}
      maxWidth="sm"
    >
      <DialogTitle id="customized-dialog-title" onClose={props.onClose}>
        Student Tracking
      </DialogTitle>
      <DialogContent>{listStudents}</DialogContent>
    </Dialog>
  );
};

export default StudentTrackingDialog;
