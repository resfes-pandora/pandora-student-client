import React, { useCallback, useRef } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import MuiDialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "./DialogTitle";
import CustomTextField from "./CustomTextField";

const DialogContent = withStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(0.5),
  },
}))(MuiDialogContent);

const useStyles = makeStyles((theme) => ({
  actions: {
    padding: "10px 0 10px 0",
    justifyContent: "center",
  },
  submitBtn: {
    color: "#FFF",
    background: "linear-gradient(to right, #FF7B55, #FF7B55)",
    fontWeight: "bold",
  },
  paper: { minWidth: "300px" },
  finishLabel: {
    paddingBottom: "7px",
  },
  finishWord: {
    color: "#FF7B55",
  },
}));

const SubmitDialog = (props) => {
  const classes = useStyles();
  const textField = useRef(null);
  const onSubmitBtnClicked = useCallback(() => {
    if ((textField.current.value === "finish")) {
      props.onSubmit();
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Dialog
      open={props.open}
      className={classes.root}
      classes={{ paper: classes.paper }}
    >
      <DialogTitle id="customized-dialog-title" onClose={props.onClose}>
        Are you sure to submit?
      </DialogTitle>
      <DialogContent>
        <h3 className={classes.finishLabel}>
          Enter "<span className={classes.finishWord}>finish</span>":
        </h3>
        <CustomTextField
          autoFocus
          size="medium"
          variant="outlined"
          inputRef={textField}
        />
      </DialogContent>
      <DialogActions className={classes.actions}>
        <Button className={classes.submitBtn} onClick={onSubmitBtnClicked}>
          SUBMIT
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SubmitDialog;
