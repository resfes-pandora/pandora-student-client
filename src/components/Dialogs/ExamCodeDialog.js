import React, { useCallback, useRef } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import MuiDialogContent from "@material-ui/core/DialogContent";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import CustomTextField from "./CustomTextField";
import DialogTitle from "./DialogTitle";

const DialogContent = withStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(0.5),
  },
}))(MuiDialogContent);

const useStyles = makeStyles((theme) => ({
  actions: {
    padding: "10px 0 10px 0",
    justifyContent: "center",
  },
  startBtn: {
    color: "#FFF",
    background: "linear-gradient(to right, #FF7B55, #FF7B55)",
    fontWeight: "bold",
  },
  paper: { minWidth: "300px" },
}));

const ExamCodeInputDialog = (props) => {
  const classes = useStyles();
  const textField = useRef(null);
  const onStartBtnClicked = useCallback(() => {
    if (textField.current.value !== "") props.onStart(textField.current.value);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Dialog
      open={props.open}
      className={classes.root}
      classes={{ paper: classes.paper }}
    >
      <DialogTitle id="customized-dialog-title" onClose={props.onClose}>
        Enter Exam Code
      </DialogTitle>
      <DialogContent>
        <CustomTextField
          autoFocus
          size="medium"
          variant="outlined"
          inputRef={textField}
        />
      </DialogContent>
      <DialogActions className={classes.actions}>
        <Button className={classes.startBtn} onClick={onStartBtnClicked}>
          START
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ExamCodeInputDialog;
