import React, { useCallback } from "react";
import { withStyles } from "@material-ui/core/styles";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";

const DialogTitle = withStyles((theme) => ({
  root: {
    padding: "10px 0 10px",
    color: "#FFF",
    backgroundColor: "#464F51",
    textAlign: "center",
  },
  closeIcon: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1.1),
    borderRadius: "5px",
    background: "linear-gradient(to right, #FF7B55, #FF7B55)",
    cursor: "pointer",
  },
}))((props) => {
  const { children, classes, onClose, ...other } = props;
  const closeDialog = useCallback(() => {
    onClose();
  }, [onClose]);
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <h3>{children}</h3>
      {onClose && (
        <CloseIcon onClick={closeDialog} className={classes.closeIcon} />
      )}
    </MuiDialogTitle>
  );
});

export default DialogTitle;
