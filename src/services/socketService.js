import io from "socket.io-client";
import {
  clientDisconnected,
  clientJoined,
  initExistingClients,
  newChatMessage,
  newPeerConnected,
  newWebCamConnected,
  newScreenConnected,
  newNotification,
  newExamStatus,
} from "../redux/actions";

let socket;
let dispatch;

export const initSocket = (initDispatch) => {
  socket = io(process.env.REACT_APP_SOCKET_HOST || "http://localhost:8001");
  // socket = io("http://localhost:8001");
  dispatch = initDispatch;

  joinRoom(initDispatch);
  registerEventHandlers();

  return socket;
};

export const getSocket = () => {
  return socket;
};

const joinRoom = (dispatch) => {
  const { user, roomId } = JSON.parse(localStorage.getItem("session"));

  socket.emit(
    "JOIN_ROOM",
    { roomId, clientName: user.name, clientType: user.type },
    (clients) => {
      console.log("Connected to room: " + roomId);

      const yourClient = clients.find((client) => client.id === socket.id);
      dispatch(initExistingClients(clients, yourClient));
    }
  );
};

export const sendChatMessage = (message) => {
  const { user, roomId } = JSON.parse(localStorage.getItem("session"));
  const data = { roomId, clientName: user.name, message: message };
  socket.emit("SEND_CHAT_MESSAGE", data);
  dispatch(newChatMessage(data));
};

export const setPeerId = (peerId) => {
  const { roomId } = JSON.parse(localStorage.getItem("session"));
  const data = { roomId, peerId };
  socket.emit("SET_PEER_ID", data);
};

export const setWebCamConnected = (clientId) => {
  const { roomId } = JSON.parse(localStorage.getItem("session"));
  const data = { roomId, clientId };
  socket.emit("WEBCAM_CONNECTED", data);
};

export const pushNotification = (notification) => {
  const { roomId } = JSON.parse(localStorage.getItem("session"));
  const data = { roomId, notification };
  socket.emit("PUSH_NOTIFICATION", data);
};

export const setScreenConnected = (clientId) => {
  const { roomId } = JSON.parse(localStorage.getItem("session"));
  const data = { roomId, clientId };
  socket.emit("SCREEN_CONNECTED", data);
};

export const updateExamStatus = (clientId, examStatus) => {
  const { roomId } = JSON.parse(localStorage.getItem("session"));
  const data = { roomId, clientId, examStatus };
  socket.emit("UPDATE_EXAM_STATUS", data);
};

const registerEventHandlers = () => {
  socket.on("NEW_NOTIFICATION", (data) => {
    dispatch(newNotification(data.notification));
  });

  socket.on("NEW_PEER_CONNECTED", (data) => {
    dispatch(newPeerConnected(data.id, data.peerId));
  });

  socket.on("NEW_WEBCAM_CONNECTED", (data) => {
    dispatch(newWebCamConnected(data.id));
  });

  socket.on("NEW_SCREEN_CONNECTED", (data) => {
    dispatch(newScreenConnected(data.id));
  });

  socket.on("NEW_CHAT_MESSAGE", (data) => {
    dispatch(newChatMessage(data));
  });

  socket.on("ANOTHER_CLIENT_JOINED", (client) => {
    dispatch(clientJoined(client));
  });

  socket.on("ANOTHER_CLIENT_DISCONNECTED", (client) => {
    dispatch(clientDisconnected(client));
  });

  socket.on("NEW_EXAM_STATUS", ({clientId, examStatus}) => {
    dispatch(newExamStatus(clientId, examStatus));
  });
};
