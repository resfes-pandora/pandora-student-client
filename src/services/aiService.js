import { pushNotification } from "./socketService";
import * as faceapi from "face-api.js";
import * as math from "mathjs";
import * as tf from "@tensorflow/tfjs";

let video, mouthModel, focusModel;

const isDoingExam = () => {
  try {
    const session = JSON.parse(localStorage.getItem("session"));

    return session.isDoingExam;
  } catch (error) {
    return false;
  }
};

const initVideo = async () => {
  video = document.createElement("video");
  video.width = 640;
  video.height = 360;
  video.autoplay = true;
  video.muted = true;

  video.srcObject = await navigator.mediaDevices.getUserMedia({
    video: {
      width: 640,
      height: 360,
      frameRate: 20,
    },
    audio: false,
  });
  await video.play();
};

const loadFaceApiModel = async () => {
  try {
    await Promise.all([
      faceapi.nets.tinyFaceDetector.loadFromUri(
        "https://justadudewhohacks.github.io/face-api.js/models"
      ),
      faceapi.nets.faceLandmark68Net.loadFromUri(
        "https://justadudewhohacks.github.io/face-api.js/models"
      ),
    ]);

    // CANVAS TO DRAW
    // const canvas = faceapi.createCanvasFromMedia(video);
    // const displaySize = { width: video.width, height: video.height };
    // faceapi.matchDimensions(canvas, displaySize);
  } catch (error) {
    console.log("Load face-api.js error", error);
  }
};

const loadTfModel = async () => {
  try {
    mouthModel = await tf.loadLayersModel(
      "https://raw.githubusercontent.com/hstdra/pandora-model/master/mouth-model/model.json"
    );
    focusModel = await tf.loadLayersModel(
      "https://raw.githubusercontent.com/hstdra/pandora-model/master/focus-model/model.json"
    );
    console.log("Tf Model Loaded!");
  } catch (error) {
    console.log("Tf Model Error", error);
  }
};

const detectFaces = async (user) => {
  const detections = await faceapi.detectAllFaces(
    video,
    new faceapi.TinyFaceDetectorOptions({
      inputSize: 320,
      scoreThreshold: 0.6,
    })
  );

  if (detections.length === 0) {
    pushNotification({ user, type: "OUT_OF_WEBCAM" });

    console.log("OUT_OF_WEBCAM");
  } else if (detections.length > 1) {
    pushNotification({ user, type: "ANOTHER_APPEAR_ON_WEBCAM" });

    console.log("ANOTHER_APPEAR_ON_WEBCAM");
  }
};

const detectLandmarks = async () => {
  try {
    const detection = await faceapi
      .detectSingleFace(
        video,
        new faceapi.TinyFaceDetectorOptions({
          inputSize: 320,
          scoreThreshold: 0.6,
        })
      )
      .withFaceLandmarks();

    if (detection) {
      const displaySize = { width: video.width, height: video.height };
      const resizedDetections = faceapi.resizeResults(detection, displaySize);

      return resizedDetections.landmarks;
    }
  } catch (error) {
    return null;
  }
};

const getCoordinate = (position) => {
  return [position.x, position.y];
};

const getDistance = (p1, p2) => {
  return math.distance(getCoordinate(p1), getCoordinate(p2));
};

const getMouthOpenScore = (positions) => {
  const topDistance = getDistance(positions[61], positions[63]);
  const bottomDistance = getDistance(positions[65], positions[67]);
  const topDownDistance = getDistance(positions[62], positions[66]);

  return (topDownDistance * 2) / (+topDistance + +bottomDistance);
};

const predictTalking = (openScores) => {
  const [acc] = mouthModel.predict(tf.tensor2d([openScores])).arraySync();

  return acc[0] > 0.7;
};

const getFocusPoints = (positions) => {
  const tuple = [
    [28, 34],
    [9, 34],
    [1, 28],
    [17, 28],
    [5, 31],
    [13, 31],
  ];

  return tuple.map((i) =>
    getDistance(positions[i[0] - 1], positions[i[1] - 1])
  );
};

function predictFocusing(positions) {
  const focusPoints = getFocusPoints(positions);
  const [acc] = focusModel.predict(tf.tensor2d([focusPoints])).arraySync();

  return acc[0] > 0.7;
}

const listenUserTalking = async (user) => {
  const openScores = [];
  let promise = Promise.resolve();

  for (let index = 0; index < 30; index++) {
    const landmarks = await detectLandmarks();

    if (!landmarks) return;

    openScores.push(getMouthOpenScore(landmarks.positions));

    promise = promise.then(function() {
      return new Promise(function(resolve) {
        setTimeout(resolve, 50);
      });
    });
  }

  promise.then(() => {
    try {
      const isTalking = predictTalking(openScores);

      // TALKING
      if (isTalking) {
        const notification = { user, type: "IS_TALKING" };
        pushNotification(notification);

        console.log("IS_TALKING");
      }
    } catch (error) {}
  });
};

const listenUserFocusing = async (user) => {
  const landmarks = await detectLandmarks();

  if (!landmarks) return;

  const isFocusing = predictFocusing(landmarks.positions);

  // TALKING
  if (!isFocusing) {
    const notification = { user, type: "IS_NOT_FOCUSING" };
    pushNotification(notification);

    console.log("IS_NOT_FOCUSING");
  }
};

export const listenAiModel = async (user) => {
  // await initVideo();
  // await loadFaceApiModel();
  // await loadTfModel();

  setTimeout(async () => {
    do {
      await listenUserTalking(user);
      await listenUserFocusing(user);
      await detectFaces(user);

      await new Promise((resolve) => {
        setTimeout(resolve, 3000);
      });
    } while (isDoingExam());
  }, 1000);

  console.log("AI MODEL STOPPED!!!");
};

export const testAiModel = async () => {
  await initVideo();
  await loadFaceApiModel();
  await loadTfModel();

  await detectLandmarks();
};
