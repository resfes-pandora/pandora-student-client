import { pushNotification } from "./socketService";
import { listenAiModel } from "./aiService";

const listenOutOfClient = async (user) => {
  document.body.addEventListener("mouseleave", () => {
    const notification = { user, type: "OUT_OF_CLIENT" };
    pushNotification(notification);

    console.log("OUT_OF_CLIENT");
  });
};

const listenUseAdditionScreen = async (user) => {
  try {
    const { desktopCapturer } = window.require("electron");

    setInterval(() => {
      desktopCapturer.getSources({ types: ["screen"] }).then((sources) => {
        if (sources.length > 1) {
          const notification = { user, type: "USE_ADDITION_SCREEN" };
          pushNotification(notification);

          console.log("USE_ADDITION_SCREEN");
        }
      });
    }, 30000);
  } catch (error) {
    console.log("DesktopCapturer not working");
  }
};

export const initCheatDetector = async (user) => {
  await listenOutOfClient(user);
  await listenUseAdditionScreen(user);
  await listenAiModel(user);
};