import Peer from "peerjs";
import { getUserWebcamStream } from "./streamService";
import { setWebCamPeerId } from "./socketService";

let peer;

export const initPeer = async (yourClient, updateClientStreams) => {
  const webCamStream = await getUserWebcamStream();
  updateClientStreams(yourClient.id, webCamStream, false);

  peer = new Peer(undefined, {
    host: "peerjs-server.herokuapp.com",
    secure: true,
    port: 443,
  });

  // peer = new Peer(undefined, {
  //   host: "localhost",
  //   port: 8002,
  //   path: "/pandora",
  //   // port: process.env.REACT_APP_PEER_PORT || 8002,
  //   // path: process.env.REACT_APP_PEER_PATH || "/pandora",
  // });

  peer.on("open", (id) => {
    setWebCamPeerId(id);
  });

  peer.on("call", (call) => {
    const { sourceClientId } = call.metadata;

    call.answer(webCamStream);

    call.on("stream", (userVideoStream) => {
      updateClientStreams(sourceClientId, userVideoStream);
    });
  });

  // clientStreams
  //   .filter((client) => client.id !== yourClient.id)
  //   .forEach((client) => {
  //     const call = peer.call(client.id, webCamStream, {
  //       metadata: {
  //         sourceClient: yourClient,
  //         type: "Webcam",
  //       },
  //     });

  //     call.on("stream", (userVideoStream) => {
  //       // client.stream = userVideoStream;
  //       updateClientStreams(client, "ADD", userVideoStream);
  //     });
  //   });

  return {
    peer,
    webCamStream,
  };
};
