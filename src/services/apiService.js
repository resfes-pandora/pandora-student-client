const axios = require("axios").create({
  baseURL: process.env.REACT_APP_API_HOST || "http://localhost:8000/",
});

export const userLogin = async (username, password) => {
  const response = await axios.post("auth/login", {
    username,
    password,
  });
  return response.data;
};

export const getExam = async (examCode) => {
  const response = await axios.get(`exams/${examCode}`).catch(() => {
    return null;
  });
  return response ? response.data : null;
};
