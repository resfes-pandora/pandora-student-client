// const { desktopCapturer } = require("electron");

export const getUserWebcamStream = async () => {
  return navigator.mediaDevices.getUserMedia({
    video: {
      width: 320,
      height: 240,
      frameRate: 20,
    },
    audio: true,
  });
};

// export const getUserScreenSources = async () => {
//   return desktopCapturer.getSources({ types: ["screen"] });
// };

// export const getUserScreenStream = async (source) => {
//   return navigator.mediaDevices.getUserMedia({
//     audio: false,
//     video: {
//       mandatory: {
//         chromeMediaSource: "desktop",
//         chromeMediaSourceId: source.id,
//         minWidth: 1280,
//         maxWidth: 1280,
//         minHeight: 720,
//         maxHeight: 720,
//       },
//     },
//   });
// };
