import React, { useState, useEffect } from "react";

const calculateSize = (width, height) => {
  const OFFSET = 1118 / 594;

  if (width / height > OFFSET) {
    return [height * OFFSET, height];
  } else {
    return [width, width / OFFSET];
  }
};

const ExamView = ({isDoingExam}) => {
  const [size, setSize] = useState([100, 50]);
  const [containerSize, setContainerSize] = useState([0, 0]);

  useEffect(() => {
    setSize(calculateSize(containerSize[0], containerSize[1]));
  }, [containerSize]);

  useEffect(() => {
    const contentArea = document.getElementById("contentArea");
    const containerWidth = contentArea.offsetWidth;
    const containerHeight = contentArea.offsetHeight;

    setContainerSize([containerWidth, containerHeight]);
  }, []);

  const imgSrc = isDoingExam ? "https://i.ibb.co/KXQZgkv/sample-exam.png" : "https://i.ibb.co/PY4X450/Logo-1.png"

  return (
    <img
      src={imgSrc}
      style={{ width: size[0], height: size[1] }}
      alt="exam_sample"
    />
  );
};

export default ExamView;
