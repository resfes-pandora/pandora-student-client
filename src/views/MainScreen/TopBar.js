import { makeStyles } from "@material-ui/core/styles";
import React, { useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  TrackingFlag,
  WebCamera,
  ExamInfo,
  StudentInfo,
  TimeCounter,
} from "../../components/TopBar";
import { goToView, finishExam } from "../../redux/actions";

const useStyles = makeStyles((theme) => ({
  container: {},
  topBar: {
    width: "100%",
    display: "flex",
    height: "100px",
    padding: ".6rem",
    justifyContent: "space-between",
    backgroundColor: "#464F51",
  },
  statusBar: {
    display: "flex",
    justifyContent: "space-around",
    width: "55rem",
    padding: ".6rem",
    margin: "0 1.2rem",
    borderRadius: "1em",
    backgroundColor: "#ABCDEF",
  },
  timeCounter: {
    display: "flex",
    margin: "-.6rem",
    padding: "1rem",
    width: "3em",
    fontSize: "xxx-large",
    color: "green",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#8aacd1",
  },
}));

const TopBar = ({ hidden }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const isDoingExam = useSelector((state) => state.session.isDoingExam);
  const examDoingTime = useSelector((state) => state.session.examDoingTime);
  const examEndTime = useSelector((state) => state.session.examEndTime);
  const user = useSelector((state) => state.session.user);

  const endExam = useCallback(() => {
    dispatch(finishExam());
  }, [dispatch]);

  const currentTime = Math.round(Date.now() / 1000);
  const doingTimeLeft =
    currentTime >= examEndTime ? 0 : examEndTime - currentTime;
  isDoingExam && dispatch(goToView("EXAM"));

  return (
    <div className={classes.topBar} hidden={hidden}>
      <TrackingFlag />
      <div className={classes.statusBar}>
        <ExamInfo totalTime={examDoingTime} />
        <div className={classes.timeCounter}>
          {user.type === "STUDENT" && isDoingExam && (
            <TimeCounter
              time={doingTimeLeft}
              isDoingExam={isDoingExam}
              endExam={endExam}
            />
          )}
        </div>
        <StudentInfo user={user} />
      </div>
      <WebCamera />
    </div>
  );
};

export default TopBar;
