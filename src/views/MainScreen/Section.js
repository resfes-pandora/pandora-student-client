import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import TopBar from "./TopBar";
import ToolBar from "./ToolBar";
import ContentArea from "./ContentArea";

const useStyles = makeStyles((theme) => ({
  mainSection: {
    display: "flex",
    flexGrow: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    overflowY: "auto",
    overflowX: "hidden",
  },
}));

const MainSection = () => {
  const classes = useStyles();

  return (
    <div className={classes.mainSection}>
      <TopBar />
      <ContentArea />
      <ToolBar />
    </div>
  );
};

export default MainSection;
