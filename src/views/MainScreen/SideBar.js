import React, { useState, useEffect, useCallback } from "react";
import { useSelector } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {
  MicControl,
  ChatBox,
  RoomMember,
  NotificationBox,
} from "../../components/SideBar";

const useStyles = makeStyles((theme) => ({
  sideBar: {
    zIndex: 3,
    width: 320,
    flex: "0 0 auto",
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#EFEFEF",
  },
  tabs: {
    height: "5%",
    backgroundColor: "#464F51",
  },
  tabPanel: {
    height: "80%",
  },
}));

const StyledTabs = withStyles({
  indicator: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent",
    "& > span": {
      maxWidth: 65,
      width: "100%",
      backgroundColor: "#00DA83",
    },
  },
})((props) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

const TwoStyledTab = withStyles((theme) => ({
  root: {
    minWidth: "160px",
    textTransform: "none",
    color: "#fff",
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(17),
    "&:focus": {
      opacity: 1,
    },
  },
}))((props) => <Tab disableRipple {...props} />);

const ThreeStyledTab = withStyles((theme) => ({
  root: {
    minWidth: "106px",
    textTransform: "none",
    color: "#fff",
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(17),
    "&:focus": {
      opacity: 1,
    },
  },
}))((props) => <Tab disableRipple {...props} />);

const TabPanel = (props) => {
  const { children, value, index } = props;
  const classes = useStyles();

  return (
    <div
      style={{ display: value !== index && "none" }}
      className={classes.tabPanel}
    >
      {value === index && children}
    </div>
  );
};

const SideBar = ({ hidden }) => {
  const classes = useStyles();
  const [activeTab, setActiveTab] = useState(0);
  const clients = useSelector((state) => state.room.clients);
  const user = useSelector((state) => state.session.user);

  const handleChange = (event, newValue) => {
    setActiveTab(newValue);
  };

  /**
   * Chat
   */
  const chatMessage = useSelector((state) => state.chat.currentMessage);
  const [unreadMessageCount, setUnreadMessageCount] = useState(0);
  const [unreadMessage, setUnreadMessage] = useState("");

  useEffect(() => {
    if (activeTab === 1) return;
    setUnreadMessageCount(unreadMessageCount + 1);
    unreadMessageCount !== 0 && setUnreadMessage(`(${unreadMessageCount})`);
  }, [chatMessage]); // eslint-disable-line react-hooks/exhaustive-deps

  const onChatTabClicked = useCallback(() => {
    setUnreadMessageCount(1);
    setUnreadMessage("");
  }, []);

  /**
   * Notification
   */
  const [unreadNotificationCount, setUnreadNotificationCount] = useState(0);
  const [unreadNotification, setUnreadNotification] = useState("");

  const notification = useSelector((state) => state.chat.currentNotification);

  useEffect(() => {
    if (activeTab === 2) return;
    setUnreadNotificationCount(unreadNotificationCount + 1);
    unreadNotificationCount !== 0 &&
      setUnreadNotification(`(${unreadNotificationCount})`);
  }, [notification]); // eslint-disable-line react-hooks/exhaustive-deps

  const onNotificationTabClicked = useCallback(() => {
    setUnreadNotificationCount(1);
    setUnreadNotification("");
  }, []);

  return (
    <div className={[classes.sideBar]} hidden={hidden}>
      <MicControl />

      {user && user.type === "SUPERVISOR" ? (
        <StyledTabs
          value={activeTab}
          className={classes.tabs}
          onChange={handleChange}
        >
          <ThreeStyledTab label={`People (${clients.length})`} />
          <ThreeStyledTab
            label={`Chat ${unreadMessage}`}
            onClick={onChatTabClicked}
          />
          <ThreeStyledTab
            label={`Noti ${unreadNotification}`}
            onClick={onNotificationTabClicked}
          />
        </StyledTabs>
      ) : (
        <StyledTabs
          value={activeTab}
          className={classes.tabs}
          onChange={handleChange}
        >
          <TwoStyledTab label={`People (${clients.length})`} />
          <TwoStyledTab
            label={`Chat ${unreadMessage}`}
            onClick={onChatTabClicked}
          />
        </StyledTabs>
      )}

      <TabPanel value={activeTab} index={0}>
        <RoomMember />
      </TabPanel>
      <ChatBox value={activeTab} index={1} />
      {user && user.type === "SUPERVISOR" && (
        <NotificationBox value={activeTab} index={2} />
      )}
    </div>
  );
};

export default SideBar;
