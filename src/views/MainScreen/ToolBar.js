import { makeStyles } from "@material-ui/core/styles";
import React, { useState, useCallback, useEffect } from "react";
import Button from "@material-ui/core/Button";
import WifiRoundedIcon from "@material-ui/icons/WifiRounded";
import WifiOffRoundedIcon from "@material-ui/icons/WifiOffRounded";
import VolumeUpRoundedIcon from "@material-ui/icons/VolumeUpRounded";
import VolumeOffRoundedIcon from "@material-ui/icons/VolumeOffRounded";
import SettingsRoundedIcon from "@material-ui/icons/SettingsRounded";
import { CustomButton } from "../../components/ToolBar";
import { useSelector, useDispatch } from "react-redux";
import { changeMuteStatus } from "../../redux/actions";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import {
  ExamCodeInputDialog,
  SubmitDialog,
  SettingDialog,
  StudentTrackingDialog,
} from "../../components/Dialogs";
import { startExam, finishExam } from "../../redux/actions";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  toolbar: {
    height: "56px",
    paddingLeft: "5px",
    paddingRight: "5px",
    color: "#FFF",
    backgroundColor: "#464F51",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  button: {
    background: "linear-gradient(to right, #FF7B55, #FF7B55)",
  },
  greenBtn: {
    background: "linear-gradient(to right, #36DE8A, #36DE8A)",
  },
  redBtn: {
    background: "linear-gradient(to right, #DD0746, #DD0746)",
  },
  icon: {
    fontSize: "27px",
  },
  label: {
    fontSize: "9px",
    fontWeight: "bold",
  },
  textButton: {
    height: "44px",
    fontWeight: "bold",
    background: "linear-gradient(to right, #FF7B55, #FF7B55)",
    margin: "0 3px",
  },
}));

const ToolBar = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const isVolumeMuted = useSelector((state) => state.setting.isVolumeMuted);
  const isOnline = useSelector((state) => state.setting.isOnline);
  const isDoingExam = useSelector((state) => state.session.isDoingExam);
  const userType = useSelector((state) => state.session.user.type);
  const [isSettingDialogShowing, setIsSettingDialogShowing] = useState(false);

  const changeMuteVolume = useCallback(() => {
    dispatch(changeMuteStatus());
  }, [dispatch]);

  /**
   * STUDENT
   */
  const [isECDialogShowing, setIsECDialogShowing] = useState(false);
  const [isSubmitDialogShowing, setIsSubmitDialogShowing] = useState(false);

  const onExamCodeInputted = useCallback(
    (examCode) => {
      dispatch(startExam(examCode));
    },
    [dispatch]
  );

  /**
   * SUPERVISOR
   */
  const [isTrackingDialogShowing, setIsTrackingDialogShowing] = useState(false);

  useEffect(() => {
    if (isDoingExam) setIsECDialogShowing(false);
  }, [isDoingExam]);

  const onSubmitExam = useCallback(() => {
    dispatch(finishExam());
    setIsSubmitDialogShowing(false);
  }, [dispatch]);

  return (
    <div className={[classes.toolbar]}>
      <div className={[classes.leftGroup]}>
        <CustomButton className={isOnline ? classes.greenBtn : classes.redBtn}>
          {isOnline ? (
            <WifiRoundedIcon className={classes.icon} />
          ) : (
            <WifiOffRoundedIcon className={classes.icon} />
          )}
          <p className={classes.label}>Wifi</p>
        </CustomButton>
        <CustomButton className={classes.button} onClick={changeMuteVolume}>
          {isVolumeMuted ? (
            <VolumeOffRoundedIcon className={classes.icon} />
          ) : (
            <VolumeUpRoundedIcon className={classes.icon} />
          )}
          <p className={classes.label}>Speaker</p>
        </CustomButton>
        <CustomButton
          className={classes.button}
          onClick={setIsSettingDialogShowing}
        >
          <SettingsRoundedIcon className={classes.icon} />
          <p className={classes.label}>Tools</p>
        </CustomButton>
        {userType === "SUPERVISOR" && (
          <CustomButton
            className={classes.button}
            onClick={setIsTrackingDialogShowing}
          >
            <FormatListBulletedIcon className={classes.icon} />
            <p className={classes.label}>Tracking</p>
          </CustomButton>
        )}
      </div>
      {userType === "STUDENT" ? (
        <>
          {isDoingExam ? (
            <Button
              variant="contained"
              color="inherit"
              className={classes.textButton}
              onClick={setIsSubmitDialogShowing}
            >
              SUBMIT
            </Button>
          ) : (
            <Button
              variant="contained"
              color="inherit"
              className={classes.textButton}
              onClick={setIsECDialogShowing}
            >
              ENTER EXAM CODE
            </Button>
          )}
          <ExamCodeInputDialog
            open={isECDialogShowing}
            onClose={setIsECDialogShowing}
            onStart={onExamCodeInputted}
          />
          <SubmitDialog
            open={isSubmitDialogShowing}
            onClose={setIsSubmitDialogShowing}
            onSubmit={onSubmitExam}
          />
        </>
      ) : (
        <StudentTrackingDialog
          open={isTrackingDialogShowing}
          onClose={setIsTrackingDialogShowing}
        />
      )}
      <SettingDialog
        open={isSettingDialogShowing}
        onClose={setIsSettingDialogShowing}
      />
    </div>
  );
};

export default ToolBar;
