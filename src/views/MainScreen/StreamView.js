import React from "react";
import StreamContainer from "../../components/StreamContainer";
import StreamProvider from "../../components/StreamContainer/StreamProvider";
import PeerProvider from "../../components/StreamContainer/PeerProvider";

const StreamView = () => {
  return (
    <StreamProvider>
      <PeerProvider>
        <StreamContainer />
      </PeerProvider>
    </StreamProvider>
  );
};

export default StreamView;
