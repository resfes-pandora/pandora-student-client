import { makeStyles } from "@material-ui/core/styles";
import React, { useEffect, useCallback } from "react";
import { useDispatch } from "react-redux";
import Section from "./Section";
import SideBar from "./SideBar";
import SocketHolder from "../../components/SocketHolder";
import { changeOnlineStatus } from "../../redux/actions";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    overflow: "hidden",
    width: "100%",
    height: "100%",
  },
}));

const MainScreen = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    updateNetwork();
  });

  const updateNetwork = useCallback(() => {
    dispatch(changeOnlineStatus(navigator.onLine));
  }, [dispatch]);

  useEffect(() => {
    window.addEventListener("online", updateNetwork);
    window.addEventListener("offline", updateNetwork);
    return () => {
      window.removeEventListener("online", updateNetwork);
      window.removeEventListener("offline", updateNetwork);
    };
  });

  return (
    <div className={classes.container}>
      <SocketHolder />
      <Section />
      <SideBar />
    </div>
  );
};

export default MainScreen;
