import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import StreamView from "./StreamView";
import ExamView from "./ExamView";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  area: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    maxWidth: "100%",
    maxHeight: "100%",
  },
}));

const ContentArea = () => {
  const classes = useStyles();
  const currentView = useSelector((state) => state.setting.currentView);
  const isDoingExam = useSelector((state) => state.session.isDoingExam);
  
  return (
    <div className={classes.root} id="contentArea">
      <div
        hidden={!["WEBCAM-SCREEN", "WEBCAM", "SCREEN"].includes(currentView)}
        className={classes.area}
      >
        <StreamView />
      </div>
      <div
        hidden={currentView !== "EXAM"}
        className={classes.area}
      >
        <ExamView isDoingExam={isDoingExam}/>
      </div>
    </div>
  );
};

export default ContentArea;
